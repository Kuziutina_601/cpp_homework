#include <iostream>
#include <cstdlib>
#include <thread>
#include "ListOfCBlocks.h"

using namespace std;

void generate(vector<unsigned char> &block) {
    for (unsigned char &ch: block) {
        ch = rand() % 255;
    }
}

void producer(ListOfCBlocks &cBlocks) {
    while (true) {
        CBlock cBlock;
        generate(cBlock.data);
        if(!cBlocks.push(cBlock)) break;
    }
}

void consumer(ListOfCBlocks &cBlocks) {
    while (cBlocks.hash()){}
}

int main() {
    ListOfCBlocks cBlocks;
    thread producer1(producer, ref(cBlocks));
    thread producer2(producer, ref(cBlocks));
    thread consumer1(consumer, ref(cBlocks));
    thread consumer2(consumer, ref(cBlocks));
    producer1.join();
    producer2.join();
    consumer1.join();
    consumer2.join();
    cBlocks.write("D:\\result.bin");
    return 0;
}