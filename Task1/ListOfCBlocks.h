//
// Created by Sofia on 28.05.2018.
//
#include <atomic>
#include <list>
#include <condition_variable>
#include "CBlock.h"

#ifndef UNTITLED2_LISTOFCBLOCKS_H
#define UNTITLED2_LISTOFCBLOCKS_H

#endif //UNTITLED2_LISTOFCBLOCKS_H

using namespace std;

class ListOfCBlocks {
    condition_variable cv;
    mutex list_mutex;
    list<CBlock> cBlocks;
    atomic<size_t> size;
    atomic<size_t> hashed;

public:
    ListOfCBlocks();
    bool hash();
    bool push(CBlock &block);
    void write(char* filename);
};