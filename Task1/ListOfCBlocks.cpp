//
// Created by Sofia on 28.05.2018.
//
#include <iostream>
#include <mutex>
#include "ListOfCBlocks.h"
#include "picosha2.h"
#include <fstream>

const int COUNT = 512;
using namespace std;

ListOfCBlocks::ListOfCBlocks() : size(0), hashed(0){}

bool ListOfCBlocks::push(CBlock &block) {
    unique_lock<mutex> u_lock(list_mutex);
    if (size > COUNT) return false;
    cBlocks.push_back(block);
    size++;
    cv.notify_all();
    u_lock.unlock();
    return true;
}

bool ListOfCBlocks::hash() {
    unique_lock<mutex> u_lock(list_mutex);
    while (size == hashed && hashed < COUNT) {
        cv.wait(u_lock);
    }
    if (hashed >= COUNT) return false;

    auto it = cBlocks.begin();
    while (it -> hash.size() != 0 && it != cBlocks.end()) it++;
    it -> hash.resize(picosha2::k_digest_size);
    picosha2::hash256(it -> data, it -> hash);
    hashed++;
    cout << "in queue " << size - hashed << ", done " << hashed << ", complete "
                                                                << hashed * 100 / COUNT << "%\n";
    u_lock.unlock();
    return hashed<COUNT;
}

void ListOfCBlocks::write(char *filename) {
    ofstream fout(filename, ios::binary);
    auto it = cBlocks.begin();
    while (it != cBlocks.end()) {
        for (unsigned char &ch: it->hash) {
            fout << ch;
        }
        it++;
        fout << "\n";
    }
    fout.close();
}