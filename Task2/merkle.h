//
// Created by Sofia on 02.06.2018.
//
#include <iostream>
#include <vector>
#include "picosha2.h"

using namespace std;

class Node{
    string data;
    Node *left, *right;
public:
    Node(vector<string> &blocks, int left_t, int right_t) {
        left = NULL;
        right = NULL;
        if (left_t == right_t) {
            data = picosha2::hash256_hex_string(blocks[left_t]);
        } else {
            int m_t = (left_t + right_t) >> 1;
            left = new Node(blocks, left_t, m_t);
            right = new Node(blocks, m_t+1, right_t);
            data = picosha2::hash256_hex_string(left->get_data()+right->get_data());
        }
    }
    ~Node() {
        if (left != NULL) delete left;
        if (right != NULL) delete right;
    }
    string get_data() {
        return data;
    }
};
