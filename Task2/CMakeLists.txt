cmake_minimum_required(VERSION 3.8)
project(Task2)

set(CMAKE_CXX_STANDARD 11)

set(SOURCE_FILES main.cpp picosha2.h merkle.h)
add_executable(Task2 ${SOURCE_FILES})