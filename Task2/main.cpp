#include <iostream>
#include <mutex>
#include <queue>
#include <condition_variable>
#include <cstdlib>
#include <fstream>
#include <thread>
#include "merkle.h"

using namespace std;

mutex list_mutex;
queue<string> queue_string;
condition_variable cv;
const int HASH_COUNT = 128;
const int HASH_SIZE = 4;
const int STRING_LENGTH = 256;

string generate() {
    string value = "";
    for (int i = 0; i < STRING_LENGTH; i++) {
        value += char('a' + rand()%26);
    }
    return value;
}

void producer() {
    int cnt = HASH_COUNT * HASH_SIZE;
    while (cnt--) {
        string str = generate();
        list_mutex.lock();
        queue_string.push(str);
        if (queue_string.size() >= HASH_SIZE) cv.notify_all();
        list_mutex.unlock();
    }
}

void ready() {
    unique_lock<mutex> u_lock(list_mutex);
    while (queue_string.size() < HASH_SIZE){
        cv.wait(u_lock);
        u_lock.unlock();
        unique_lock<mutex> u_lock(list_mutex);
    }

}

void consumer() {
    int cnt = HASH_COUNT;
    ofstream fout("D:\\log.txt");
    while (cnt--) {
        ready();

        vector<string> blocks;
        for (int i = 0; i < HASH_SIZE; i++) {
            blocks.push_back(queue_string.front());
            queue_string.pop();
        }
        Node *node = new Node(blocks, 0, HASH_SIZE - 1);
        fout << node->get_data() << "\n";
        delete node;
    }
}

int main() {
    thread consumer(consumer);
    thread producer(producer);
    consumer.join();
    producer.join();
    return 0;
}